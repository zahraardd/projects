<?php
class MyDb{
	
    private $mysqli;
    
    private $lastQuery;
    
	public function __construct($hostname, $user, $pass, $db) {
    
      $this->mysqli = new mysqli($hostname, $user, $pass, $db);
        
      if ($this->mysqli->connect_errno) {
        echo("Connetion error");
        exit();
      }
	}
    
	public function query($sqlQuery){
      $this->lastQuery = $sqlQuery;
	  return $this->mysqli->query($sqlQuery);
	}
    
    public function lastQuery() {
      return $this->lastQuery;
    }
    
    public function getMySQLi() {
      return $this->mysqli;
    }
    
    public function __destruct() {
      $this->mysqli->close();      
    }
}


$db = new MyDb("localhost","root","","data1");
$result = $db->query("SELECT * FROM students");